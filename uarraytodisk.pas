unit uarraytodisk;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Dialogs;

type
  TFileOF=(foInteger, foCurrency, foText, foBoolean);

type
  TIntArray=array of Integer;
  TCurrArray=array of Currency;
  TTextArray=array of String;
  TBoolArray=array of Boolean;

  function ArrayToFile(IntArray:array of Integer; aFileName:String):Word;
  function ArrayToFile(CurrArray:array of Currency; aFileName:String):Word;
  function ArrayToFile(strArray:array of String; aFileName:String):Word;
  function ArrayToFile(BoolArray:array of Boolean; aFileName:String):Word;
  function ArrayLoadFromFile(var IntArray:TIntArray; aFileName:String):Word;
  function ArrayLoadFromFile(var CurrArray:TCurrArray; aFileName:String):Word;
  function ArrayLoadFromFile(var strArray:TTextArray; aFileName:String):Word;
  function ArrayLoadFromFile(var BoolArray:TBoolArray; aFileName:String):Word;
  function CheckFileName(f:String; WR:Boolean):Word; //WR-->True Open to write False Open to read.

implementation

function ArrayToFile(IntArray: array of Integer; aFileName: String): Word;
var
  i:Integer;
  aFile:file of Integer;
begin
  Result:=CheckFileName(aFileName,True);
  if Result<>0 then Exit;
  AssignFile(aFile,aFileName);
  Rewrite(aFile);
  for i:=Low(IntArray) to High(IntArray) do Write(aFile,IntArray[i]);
  CloseFile(aFile);
end;

function ArrayToFile(CurrArray: array of Currency; aFileName: String): Word;
var
  i:Integer;
  aFile:file of Currency;
begin
  Result:=CheckFileName(aFileName,True);
  if Result<>0 then Exit;
  AssignFile(aFile,aFileName);
  Rewrite(aFile);
  for i:=Low(CurrArray) to High(CurrArray) do Write(aFile,CurrArray[i]);
  CloseFile(aFile);
end;

function ArrayToFile(strArray: array of String; aFileName: String): Word;
var
  i:Integer;
  aFile:TextFile;
begin
  Result:=CheckFileName(aFileName,True);
  if Result<>0 then Exit;
  AssignFile(aFile,aFileName);
  Rewrite(aFile);
  for i:=Low(strArray) to High(strArray) do Write(aFile,strArray[i]);
  CloseFile(aFile);
end;

function ArrayToFile(BoolArray: array of Boolean; aFileName: String): Word;
var
  i:Integer;
  aFile:file of Boolean;
begin
  Result:=CheckFileName(aFileName,True);
  if Result<>0 then Exit;
  AssignFile(aFile,aFileName);
  Rewrite(aFile);
  for i:=Low(BoolArray) to High(BoolArray) do Write(aFile,BoolArray[i]);
  CloseFile(aFile);
end;

function ArrayLoadFromFile(var IntArray: TIntArray; aFileName: String ): Word;
var
  i,n:Integer;
  aFile:file of Integer;
begin
  Result:=CheckFileName(aFileName,False);
  if Result<>0 then Exit;
  SetLength(IntArray,0);
  AssignFile(aFile,aFileName);
  Reset(aFile);
  n:=FileSize(aFile);
  SetLength(IntArray,n);
  for i:=1 to n do Read(aFile,IntArray[i-1]);
  CloseFile(aFile);
end;

function ArrayLoadFromFile(var CurrArray: TCurrArray; aFileName: String ): Word;
var
  i,n:Integer;
  aFile:file of Currency;
begin
  Result:=CheckFileName(aFileName,False);
  if Result<>0 then Exit;
  SetLength(CurrArray,0);
  AssignFile(aFile,aFileName);
  Reset(aFile);
  n:=FileSize(aFile);
  SetLength(CurrArray,n);
  for i:=1 to n do Read(aFile,CurrArray[i-1]);
  CloseFile(aFile);
end;

function ArrayLoadFromFile(var strArray: TTextArray; aFileName: String ): Word;
var
  i,n:Integer;
  sl:TStringList;
begin
  Result:=CheckFileName(aFileName,False);
  if Result<>0 then Exit;
  SetLength(strArray,0);
  sl:=TStringList.Create;
  sl.LoadFromFile(aFileName);
  n:=sl.Count;
  SetLength(strArray,n);
  for i:=0 to n-1 do
    strArray[i]:=sl[i];
  sl.Free;
end;

function ArrayLoadFromFile(var BoolArray: TBoolArray; aFileName: String ): Word;
var
  i,n:Integer;
  aFile:file of Boolean;
begin
  Result:=CheckFileName(aFileName,False);
  if Result<>0 then Exit;
  SetLength(BoolArray,0);
  AssignFile(aFile,aFileName);
  Reset(aFile);
  n:=FileSize(aFile);
  SetLength(BoolArray,n);
  for i:=1 to n do Read(aFile,BoolArray[i-1]);
  CloseFile(aFile);
end;

function CheckFileName(f: String; WR: Boolean): Word;
var
  diskfile:file;
begin
  AssignFile(diskfile,f);
  {$I-}
  if WR then Rewrite(diskfile) else Reset(diskfile);
  {$I+}
  Result:=IOResult;
  CloseFile(diskfile);
end;

end.

